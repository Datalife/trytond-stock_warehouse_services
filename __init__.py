# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import account
from . import product
from . import stock


def register():
    Pool.register(
        account.InvoiceLine,
        product.Template,
        product.Product,
        product.WarehouseService,
        stock.Location,
        stock.Move,
        module='stock_warehouse_services', type_='model')
