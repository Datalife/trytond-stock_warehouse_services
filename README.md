datalife_stock_warehouse_services
=================================

The stock_warehouse_services module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-stock_warehouse_services/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-stock_warehouse_services)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
