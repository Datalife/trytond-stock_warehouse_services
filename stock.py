# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from decimal import Decimal
from functools import wraps

from trytond.model import fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval
from trytond.transaction import Transaction

__all__ = ['Location', 'Move']


class Location(metaclass=PoolMeta):
    __name__ = 'stock.location'

    service_party = fields.Many2One('party.party', 'Service Party',
        states={
            'invisible': (Eval('type') != 'warehouse')
        },
        depends=['type'],
        help='The party invoiced when warehouse services are used.')


def set_origin_warehouse_service(func):
    @wraps(func)
    def wrapper(cls, moves):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        lines = []
        for move in moves:
            invoice_lines = InvoiceLine.search([
                ('invoice_type', '=', 'in'),
                ('type', '=', 'line'),
                ('origin', '=', 'stock.move,%s' % move.id),
                ('product.type', '=', 'service')], limit=1)
            if not invoice_lines:
                invoice_lines = move.get_invoice_lines_warehouse_service()
                if invoice_lines:
                    lines.extend(invoice_lines)
        if lines:
            InvoiceLine.save(lines)
        return func(cls, moves)
    return wrapper


def unset_origin_warehouse_service(func):
    @wraps(func)
    def wrapper(cls, moves):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        lines = []
        for move in moves:
            invoice_lines = InvoiceLine.search([
                ('invoice_type', '=', 'in'),
                ('type', '=', 'line'),
                ('origin', '=', 'stock.move,%s' % move.id),
                ('product.type', '=', 'service')])
            if invoice_lines:
                lines.extend(invoice_lines)
        if lines:
            InvoiceLine.delete(lines)
        return func(cls, moves)
    return wrapper


class Move(metaclass=PoolMeta):
    __name__ = 'stock.move'

    def _get_tax_rule_pattern(self):
        return {}

    def _get_warehouse_service(self):
        loc_type = {
            'storage': self.from_location,
            'production': self.to_location
        }
        loc = loc_type.get(self.from_location.type, None)
        if loc:
            return loc.warehouse
        return None

    def get_invoice_lines_warehouse_service(self):
        if not self.product.warehouse_services:
            return

        wh = self._get_warehouse_service()
        if (wh and wh.service_party and
                self.from_location.warehouse != self.to_location.warehouse and
                self.to_location.type not in ('supplier', 'production')):
            return self._get_supplier_invoice_lines_warehouse_service()

    def _get_warehouse_service_pattern(self):
        wh = self._get_warehouse_service()
        return {
            'type_': self.from_location.type,
            'warehouse': wh and wh.id or None
        }

    def _get_supplier_invoice_lines_warehouse_service(self):
        pool = Pool()
        InvoiceLine = pool.get('account.invoice.line')
        Product = pool.get('product.product')
        ProductSupplier = pool.get('purchase.product_supplier')

        wh = self._get_warehouse_service()
        with Transaction().set_context(
                supplier=wh.service_party.id):
            pattern = ProductSupplier.get_pattern()
        for product_supplier in self.product.product_suppliers:
            if product_supplier.match(pattern):
                currency = product_supplier.currency
                break
        else:
            currency = self.company.currency

        pattern = self._get_warehouse_service_pattern()

        lines = []
        for service in self.product.warehouse_services:
            if not service.match(pattern):
                continue

            product = service.service
            line = InvoiceLine()
            line.invoice_type = 'in'
            line.type = 'line'
            line.company = self.company
            line.party = wh.service_party
            line.currency = currency
            line.product = product
            line.description = product.name
            line.quantity = self.quantity
            line.unit = product.default_uom
            line.account = product.account_expense_used
            line.origin = self

            taxes = []
            tax_pattern = self._get_tax_rule_pattern()
            for tax in line.product.supplier_taxes_used:
                if line.party.supplier_tax_rule:
                    tax_ids = line.party.supplier_tax_rule.apply(
                        tax, tax_pattern)
                    if tax_ids:
                        taxes.extend(tax_ids)
                    continue
                taxes.append(tax.id)
            if line.party.supplier_tax_rule:
                tax_ids = line.party.supplier_tax_rule.apply(None, tax_pattern)
                if tax_ids:
                    taxes.extend(tax_ids)
            line.taxes = taxes

            with Transaction().set_context(
                    currency=line.currency.id,
                    supplier=line.party.id,
                    uom=line.unit,
                    taxes=[t.id for t in line.taxes]):
                line.unit_price = Product.get_purchase_price(
                    [line.product], line.quantity)[line.product.id]
                line.unit_price = line.unit_price.quantize(
                    Decimal(1) / 10 ** line.__class__.unit_price.digits[1])

            lines.append(line)

        return lines

    @classmethod
    @unset_origin_warehouse_service
    def draft(cls, moves):
        super(Move, cls).draft(moves)

    @classmethod
    @set_origin_warehouse_service
    def assign(cls, moves):
        super(Move, cls).assign(moves)

    @classmethod
    @set_origin_warehouse_service
    def do(cls, moves):
        super(Move, cls).do(moves)

    @classmethod
    @unset_origin_warehouse_service
    def cancel(cls, moves):
        super(Move, cls).cancel(moves)
