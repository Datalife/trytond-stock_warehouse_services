=================================
Stock Warehouse Services Scenario
=================================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_chart, \
    ...     get_accounts, create_tax
    >>> today = datetime.date.today()

Install stock_warehouse_services::

    >>> config = activate_modules('stock_warehouse_services')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> revenue = accounts['revenue']
    >>> expense = accounts['expense']

Create tax::

    >>> supplier_tax = create_tax(Decimal('.10'))
    >>> supplier_tax.save()
    >>> customer_tax = create_tax(Decimal('.10'))
    >>> customer_tax.save()

Create account categories::

    >>> ProductCategory = Model.get('product.category')
    >>> account_category = ProductCategory(name="Account Category")
    >>> account_category.accounting = True
    >>> account_category.account_expense = expense
    >>> account_category.account_revenue = revenue
    >>> account_category.save()
    >>> account_category_tax, = account_category.duplicate()
    >>> account_category_tax.supplier_taxes.append(supplier_tax)
    >>> account_category_tax.customer_taxes.append(customer_tax)
    >>> account_category_tax.save()

Create parties::

    >>> Party = Model.get('party.party')
    >>> supplier = Party(name='Supplier of services')
    >>> supplier.save()
    >>> customer = Party(name='Customer')
    >>> customer.save()

Get stock locations::

    >>> Location = Model.get('stock.location')
    >>> warehouse_loc, = Location.find([('code', '=', 'WH')])
    >>> supplier_loc, = Location.find([('code', '=', 'SUP')])
    >>> storage_loc, = Location.find([('code', '=', 'STO')])
    >>> customer_loc, = Location.find([('code', '=', 'CUS')])
    >>> output_loc, = Location.find([('code', '=', 'OUT')])
    >>> lost_loc, = Location.find([('type', '=', 'lost_found')])

Create a second warehouse::

    >>> warehouse_loc2, = warehouse_loc.duplicate()

Create product::

    >>> ProductUom = Model.get('product.uom')
    >>> unit, = ProductUom.find([('name', '=', 'Unit')])
    >>> ProductTemplate = Model.get('product.template')
    >>> Product = Model.get('product.product')
    >>> product = Product()
    >>> ptemplate = ProductTemplate()
    >>> ptemplate.name = 'product'
    >>> ptemplate.default_uom = unit
    >>> ptemplate.type = 'goods'
    >>> ptemplate.purchasable = True
    >>> ptemplate.list_price = Decimal('10')
    >>> ptemplate.account_category = account_category_tax
    >>> ptemplate.save()
    >>> product, = ptemplate.products

Create services::

    >>> Tax = Model.get('account.tax')
    >>> supplier_tax = Tax(supplier_tax.id)
    >>> customer_tax = Tax(customer_tax.id)
    >>> ProductTemplate = Model.get('product.template')
    >>> template = ProductTemplate()
    >>> template.name = 'Service 1'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('5')
    >>> template.account_category = account_category_tax
    >>> product_supplier = template.product_suppliers.new()
    >>> product_supplier.party = supplier
    >>> price = product_supplier.prices.new()
    >>> price.quantity = 2
    >>> price.unit_price = Decimal('4')
    >>> template.save()
    >>> service1, = template.products
    >>> template = ProductTemplate()
    >>> template.name = 'Service 2'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('7.5')
    >>> supplier_tax = Tax(supplier_tax.id)
    >>> customer_tax = Tax(customer_tax.id)
    >>> template.account_category = account_category_tax
    >>> product_supplier = template.product_suppliers.new()
    >>> product_supplier.party = supplier
    >>> price = product_supplier.prices.new()
    >>> price.quantity = 2
    >>> price.unit_price = Decimal('6')
    >>> template.save()
    >>> service2, = template.products
    >>> template = ProductTemplate()
    >>> template.name = 'Service 3 for production'
    >>> template.default_uom = unit
    >>> template.type = 'service'
    >>> template.purchasable = True
    >>> template.list_price = Decimal('9.5')
    >>> supplier_tax = Tax(supplier_tax.id)
    >>> customer_tax = Tax(customer_tax.id)
    >>> template.account_category = account_category_tax
    >>> product_supplier = template.product_suppliers.new()
    >>> product_supplier.party = supplier
    >>> price = product_supplier.prices.new()
    >>> price.quantity = 2
    >>> price.unit_price = Decimal('6')
    >>> template.save()
    >>> service3, = template.products

Add services to product::

    >>> serv = ptemplate.warehouse_services.new()
    >>> serv.service = service1
    >>> serv.warehouse = warehouse_loc
    >>> serv = ptemplate.warehouse_services.new()
    >>> serv.service = service2
    >>> serv = ptemplate.warehouse_services.new()
    >>> serv.type_ = 'production'
    >>> serv.service = service3
    >>> serv = ptemplate.warehouse_services.new()
    >>> serv.service = service3
    >>> serv.warehouse = warehouse_loc2
    >>> ptemplate.save()

Configure warehouse::

    >>> warehouse_loc.service_party = supplier
    >>> warehouse_loc.save()

Create a shipment out::

    >>> ShipmentOut = Model.get('stock.shipment.out')
    >>> shipment_out = ShipmentOut()
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> move = shipment_out.outgoing_moves.new()
    >>> move.product = product
    >>> move.quantity = 3
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> shipment_out.click('wait')
    >>> shipment_out.click('assign_force')
    >>> shipment_out.click('pack')
    >>> move, = shipment_out.outgoing_moves

Check supplier invoice lines::

    >>> Invoiceline = Model.get('account.invoice.line')
    >>> lines = Invoiceline.find([])
    >>> len(lines)
    2
    >>> list(set(str(l.origin) for l in lines)) == [str(move)]
    True
    >>> all(l.product.id in (service1.id, service2.id) for l in lines)
    True
    >>> line, = [l for l in lines if l.product.id == service1.id]
    >>> line.quantity
    3.0
    >>> line.unit_price
    Decimal('4.0000')

Cancel supplier warehouse service::

    >>> shipment_out = ShipmentOut()
    >>> shipment_out.customer = customer
    >>> shipment_out.warehouse = warehouse_loc
    >>> move = shipment_out.outgoing_moves.new()
    >>> move.product = product
    >>> move.quantity = 1
    >>> move.from_location = output_loc
    >>> move.to_location = customer_loc
    >>> shipment_out.click('wait')
    >>> shipment_out.click('assign_force')
    >>> shipment_out.click('pack')
    >>> move, = shipment_out.outgoing_moves
    >>> not Invoiceline.find([('origin', '=', 'stock.move,%s' % move.id)])
    False
    >>> shipment_out.click('cancel')
    >>> shipment_out.state
    'cancel'
    >>> not Invoiceline.find([('origin', '=', 'stock.move,%s' % move.id)])
    True

Create Components for a production::

    >>> template1 = ProductTemplate()
    >>> template1.name = 'component 1'
    >>> template1.default_uom = unit
    >>> template1.type = 'goods'
    >>> template1.list_price = Decimal(5)
    >>> component1, = template1.products
    >>> component1.cost_price = Decimal(1)
    >>> template1.save()
    >>> component1, = template1.products

    >>> meter, = ProductUom.find([('name', '=', 'Meter')])
    >>> centimeter, = ProductUom.find([('name', '=', 'centimeter')])

    >>> template2 = ProductTemplate()
    >>> template2.name = 'component 2'
    >>> template2.default_uom = meter
    >>> template2.type = 'goods'
    >>> template2.list_price = Decimal(7)
    >>> component2, = template2.products
    >>> component2.cost_price = Decimal(5)
    >>> template2.save()
    >>> component2, = template2.products

Create Bill of Material::

    >>> BOM = Model.get('production.bom')
    >>> BOMInput = Model.get('production.bom.input')
    >>> BOMOutput = Model.get('production.bom.output')
    >>> bom = BOM(name='product')
    >>> input1 = BOMInput()
    >>> bom.inputs.append(input1)
    >>> input1.product = component1
    >>> input1.quantity = 5
    >>> input2 = BOMInput()
    >>> bom.inputs.append(input2)
    >>> input2.product = component2
    >>> input2.quantity = 150
    >>> input2.uom = centimeter
    >>> output = BOMOutput()
    >>> bom.outputs.append(output)
    >>> output.product = product
    >>> output.quantity = 1
    >>> bom.save()

    >>> product.template.producible = True
    >>> product.template.save()
    >>> ProductBom = Model.get('product.product-production.bom')
    >>> product.boms.append(ProductBom(bom=bom))
    >>> product.save()

Make a production::

    >>> Production = Model.get('production')
    >>> production = Production()
    >>> production.planned_date = today
    >>> production.warehouse = warehouse_loc
    >>> production.product = product
    >>> production.bom = bom
    >>> production.quantity = 2
    >>> output, = production.outputs
    >>> production.click('wait')
    >>> production.click('assign_force')
    >>> production.click('run')
    >>> production.click('done')

Check production service::

    >>> move, = production.outputs
    >>> lines = Invoiceline.find([])
    >>> len(lines)
    3
    >>> lines = Invoiceline.find([('origin', '=', 'stock.move,%s' % move.id)])
    >>> len(lines)
    1
    >>> lines[0].product == service3
    True